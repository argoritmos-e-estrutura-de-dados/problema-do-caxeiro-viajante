LIBS=-Wall -std=c++11 -w
FILES=$(shell find ./ -name "*.cpp")
OBJECTS=$(shell find -name "*.o")
EXEC=tp1
all: objects $(EXEC)
.PHONY: uninstall install clean mrproper reinstall help
help:
	echo "reinstall,install,uninstall,clean,mrproper"
reinstall: uninstall install
uninstall:
	rm -r 
install: all
	cp $(EXEC) $(bindir)
$(EXEC): $(OBJECTS)
	g++ $^ -o $@ $(CFLAGS) $(LIBS)
objects: $(FILES)
	g++ $^ -c $(CFLAGS) $(LIBS)
clean:
	rm -f ./*.o
mrproper: uninstall
	rm -f $(EXEC)
