#include <iostream>
#include <fstream>
#include <chrono>
#include <math.h>


#define INTERGER_MAX_VALUE 999999

//Calcula o número máximo de permutações possíveis considerando f números
int factorial(int f){
    if(f == 0) return 1;
    return (f*factorial(f-1));
}

//Realiza a permutação dos elementos de uma lista
class Permutation{
public:
    Permutation(int size): p_size(size), p_iterator(0){
        p_permutation_list = new int[size*factorial(size)];
    }
    virtual ~Permutation(){}
    
    void permute(int list[], int bottom, int higher){
        if(bottom == higher){
            for(int j = 0; j <= higher; j++){
                p_permutation_list[p_iterator*p_size+j] = list[j];                
            }
            p_iterator++;
        }else
            for(int i=bottom; i <= higher; i++){                
                swap(list, bottom, i);
                permute(list, bottom+1, higher);
                swap(list, bottom, i);
        }
    }
    
    int *p_permutation_list;
    int p_size, p_iterator;
    
private:
    void swap(int list[], int i, int j){
        int variable = list[i];
        list[i] = list[j];
        list[j] = variable;
    }
};

class NearestNeighbor{
private:
    float* nn_path; /** Armazena o menor caminho percorrido possível. */
    float* nn_weight; /** Armazena o peso do menor caminho percorrido possível. */
    int nn_length; /** Armazena o tamanho da matriz. */    
    
public:
    NearestNeighbor(float* distance_matrix, int starting_city, int length):
    nn_length(length){
        nn_path = new float[nn_length+1];
        nn_weight = new float[nn_length+1];
        
        /** Inicializa a matriz que conterá o caminho mínimo com o "valor" máximo,
         * uma vez que o problema utiliza-se de comparações entre tamanhos para
         * calcular o menor caminho possível. */
        for (int j = 0; j < nn_length; j++){
            nn_path[j] = INTERGER_MAX_VALUE;
            nn_weight[j] = 0;
        }
        
        /** Define a cidade inicial, escolhida pelo usuário. */
        nn_path[0] = starting_city;
        int current_city = starting_city;
        
        int i = 1;
        /** Enquanto existirem cidades a serem visitadas pelo caixeiro. */
        while (i < nn_length) {
            /** Define a próxima cidade a ser visitada. */
            int next_city = findMinimumWay(&distance_matrix[current_city]);
            /** Se a cidade ainda não foi visitada. */
            if (next_city != -1) {                
                /** Adiciona a cidade ao caminho. */
                nn_path[i] = next_city;                
                nn_weight[i] = distance_matrix[current_city*nn_length+next_city];
                /** Atualiza a cidade atual e o contador. */
                current_city = next_city;                
                i++;
            }
        }
        nn_path[i] = starting_city;
        nn_weight[i] = distance_matrix[current_city*nn_length+starting_city];
    }
    
    /** Define o menor custo para o caminho passado como parâmetro. */
    int findMinimumWay(float* minimum_way){
        /** Define o menor custo para o caminho passado como parâmetro. */
        float next_city = -1;
        int i = 0;
        float minimum_value = INTERGER_MAX_VALUE;
        
        /** Enquanto existirem cidades a serem visitadas pelo caixeiro. */
        while (i < nn_length) {
            /** Se a cidade ainda não foi visitada e seu custo é menor que o custo
             definido pela* variável minimun_value, minimun_value recebe este novo
             custo, e, o caminho é iterado para a próxima cidade. */
            if (!isCityInPath(nn_path, i) && minimum_way[i] < minimum_value) {
                minimum_value = minimum_way[i];
                next_city = i;
            }
            i++;
        }
        return next_city;
    }
    /** Verifica se a cidade encontra-se no caminho definido por parâmetro. */
    bool isCityInPath(float* path, int city){
        for (int i = 0; i < nn_length; i++) {
            if (path[i] == city) {
                return true;
            }
        }
        return false;
    }
    
    /** Retorna o caminho mínimo percorrido pelo caixeiro. */
    inline float* getPath(){ return nn_path; }
    /** Retorna os pesos do caminho mínimo percorrido pelo caixeiro. */
    inline float* getWeight(){ return nn_weight; }
    /** Retorna o tamanho da matriz. */
    inline int getN(){ return nn_length; }

};

int main(int argc, char *argv[]){
    int n;          //Define o número de cidades da matriz
    float x, y;     //Define as coordenadas das cidades na matriz
    int city;       //Define o número que identifica a cidade
    int register i, j;
    
    
    clock_t brute_force_clock_start, brute_force_clock_end,
    nearest_neighbor_clock_start, nearest_neighbor_clock_end;
    
    if(argc < 3){
        std::cout << "Número de parâmetros inválidos.";
    }else{
        //Atribui o identificador que nos permite acesso aos dados
        std::fstream input_file;
        input_file.open(argv[1], std::fstream::in);
        
        //Se o arquivo de entrada abrir corretamente
        if(input_file.is_open() && input_file.good()){
            
            input_file >> n;
            
            //Matriz que define as coordenas euclidiana das cidades
            float euclidean_coordinates[n][2];
            //Leitura do arquivo de entrada com a matriz euclidiana
            for(i = 0; i < n; i++){
                input_file >> city >> x >> y;
                euclidean_coordinates[city-1][0] = x;
                euclidean_coordinates[city-1][1] = y;
            }
            //Fecha o arquivo de entrada
            input_file.close();
            
            //Matriz de adjacência que define as distâncias entre as cidades
            float *adjacency_matrix = new float[n*n];
            
            //Cálculo da matriz de adjacência baseado no principio da desigualdade triangular
            for(i = 0; i < n; i++){
                for(j = 0; j < n; j++){
                    if(i == j) adjacency_matrix[i*n+j] = 0;
                    else adjacency_matrix[i*n+j] = sqrt(
                                    pow((euclidean_coordinates[i][0]-euclidean_coordinates[j][0]),2) +
                                    pow((euclidean_coordinates[i][1]-euclidean_coordinates[j][1]),2));
                }
            }
            /** Inicia a pesquisa utilizando o algoritmo de Força Bruta. */
            brute_force_clock_start = clock();
                        
            //Lista com todas as permutações possíveis considerando a condição de não repetição de ciclos idênticos
            int list_for_permutation[n];
            //Define um lista de todas as cidade para permutação
            for(i = 0; i < n;i++) list_for_permutation[i] = i;
            
            Permutation *p = new Permutation(n);
            p->permute(list_for_permutation, 1, n-1);
            
            std::fstream output_file;
            output_file.open(argv[2], std::fstream::out);
           
            float minimum_coast = INTERGER_MAX_VALUE;
            //Se o arquivo de saída abrir corretamente
            if(output_file.is_open() && output_file.good()){
                float coast;
                output_file << n << std::endl;
                for(i = 0; i < p->p_iterator; i++){
                    coast = 0;       
                    for(j = 0; j < p->p_size; j++){
                        output_file << p->p_permutation_list[i*p->p_size+j] << " ";
                        coast += adjacency_matrix[(p->p_permutation_list[i*p->p_size+j])*n+(p->p_permutation_list[i*p->p_size+j+1])];
                    }
                    if(minimum_coast > coast){
                        minimum_coast = coast;
                    }
                    output_file << "= $" << coast << std::endl;
                }
                std::cout << "Custo mínimo para Força Bruta = $ " << minimum_coast << std::endl;
                output_file.close();
                
                brute_force_clock_end = clock();
                
                /** Encerra a pesquisa utilizando o algoritmo de Força Bruta. */
            }else{
                std::cout << "Falha ao carregar arquivo de saída." << std::endl;
            }
            
            /** Inicia a pesquisa utilizando a heurística de Vizinho mais Próximo. */
            nearest_neighbor_clock_start = clock();
            
            NearestNeighbor* nn = new NearestNeighbor(adjacency_matrix, 0, n);
            float nearest_neighbor_coast = 0;
                float *path = (*nn).getPath(), *weight = (*nn).getWeight();
                
                for(int i = 0; i <= n; i++){
                    // Calcula a distância total percorrida pelo algoritmo de vizinho mais próximo.
                    nearest_neighbor_coast += weight[i];
                    // Exibe o caminho percorrido utilizando o algoritmo de vizinho mais próximo.
                    std::cout << path[i] << " ";
                }
                std::cout << "= $" << nearest_neighbor_coast << std::endl;
                
                nearest_neighbor_clock_end = clock();
                
                /** Encerra a pesquisa utilizando a heurística de Vizinho mais Próximo. */
                
                std::cout << "Tempo de Execução: " << std::endl <<
                "\tBF: " << (((double)brute_force_clock_end - brute_force_clock_start) / (CLOCKS_PER_SEC) / (double)1000) << " segundos" << std::endl <<
                "\tNN: " << (((double)nearest_neighbor_clock_end - nearest_neighbor_clock_start) / (CLOCKS_PER_SEC) / (double)1000) << " segundos" << std::endl;
            
                std::cout << "Otimalidade(G): "  << (nearest_neighbor_coast - minimum_coast)/minimum_coast << std::endl;
            return 0;
        }
        else{
            std::cout << "Falha ao carregar arquivo de entrada.";
            return 1;
        }
    }
}
