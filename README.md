# Problema do Caixeiro Viajante

### Definição
O Problema do Caixeiro Viajante (do inglês Travelling Salesman Problem -TSP1) tem como objetivo determinar a menor rota para percorrer um conjunto de cidades, retornando sempre à cidade de origem. Esta rota é denominada ciclo hamiltoniano.

O Problema do Caixeiro Viajante Euclidiano é um caso particular do PCV onde as distâncias entre as cidades estão dadas por pontos num plano e obedecem a desigualdade triangular.


## Parte I
Escreva uma função que recebe um vetor com as coordenadas dos pontos de um conjunto de cidades e imprime todos os possíveis ciclos de PCV Euclidiano. Você não
deve repetir ciclos idênticos, mas que comecem por cidades diferentes. Por exemplo, o ciclo do exemplo 2, `BSB – BH – RJ – SP – BSB` é igual ao ciclo `BH – RJ – SP – BSB – BH`. Calcule a complexidade assintótica no pior caso de sua implementação.

### Arquivo de entrada
A primeira linha do arquivo de entrada tem o número de cidades (n). A primeira coluna contém um número que identifica a cidade. Na segunda coluna contém a coordenada X das cidades e na terceira coluna a coordenada Y. O exemplo a seguir ilustra o arquivo de entrada.
```
    n
    1 x1 y1
    2 x2 y2
    3 x3 y3
    4 x4 y4
    ...
```
### Arquivo de saída
A primeira linha do arquivo de saída tem o número ciclos gerados (m). Cada linha deverá conter um possível ciclo do PCV euclidiado mostrando as cidades pertencentes ao ciclo separadas por espaço e ao final o custo total do ciclo. O exemplo a seguir ilustra o arquivo de saída.
```
    m
    1 2 3 4 ... 1 = $custo
    2 1 3 4 ... 2 = $custo
    4 2 1 3 ... 4 = $custo
    ...
```

## Parte II
Dado o conjunto de instâncias (casos de teste) em anexo, com número de cidades igual a `6, 7, 8, 9, 10, 11, 12, 13, 14, ...`. Mostre uma solução ótima (aquela com menor custo) para cada instância e plote um gráfico do tempo de execução do seu algoritmo em função do número de cidades da instância.


## Parte III
Você deve solucionar o PCV usando a heurística do vizinho mais próximo (do inglês, Nearest Neighbor), que consiste em iniciar o ciclo em uma cidade e continuar com a cidade mais próxima ainda não visitada. Você também deve calcular a complexidade assintótica no pior caso desta heurística e fazer uma tabela de comparação entre a solução da heurística e a solução ótima com as instâncias da parte II. A comparação é feita da calculando-se o gap de otimalidade (G) entre a solução ótima S* esolução da heurística S:
    `G = (S − S*)/(S*)`
Por exemplo, se o ciclo ótimo tem custo 22,08 e o ciclo que a heurística retorna tem custo 23,03.
O gap da heurística nesta instância é:
    `G = 23,03 − 22,08/22,08 ≈ 0,04 = 4%`.

### Entrega
A entrega se divide em duas partes, que devem ser submetidas separadamente no Moodle:
    (i) O aluno deve entregar o código, com um arquivo Makefile ou Execute, contendo todos os comandos que compilam o código. A linguagem para realizar este Trabalho é C ou C++. Se quiserem usar outra linguagem devem consultar previamente o professor. Seu arquivo executável deve-se chamar tp1 e deve ser chamado da seguinte maneira:
        `./tp1 <arquivo de entrada> <arquivo de saída>`
    (ii) O relatório deve estar em formato PDF e dividido em três sessões, cada uma delas contendo o que foi pedido em cada uma das três partes do TP.

