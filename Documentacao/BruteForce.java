package BruteForce;

import Graph.JGraph;

/** @author messiah */
public class BruteForce {
    final int MIN = 99999;
    
    int weight[][];
    int n, final_cost;
    int [] tour;
    
    public BruteForce(JGraph jg){
        this.n = jg.getN();
        this.weight = new int[n][n];
        this.weight = jg.getE();
        this.tour = new int[this.n-1];
    }
    
    public void search(){
        int[] path = new int[this.n-1];
        for(int i = 1; i < this.n; i++){
            path[i-1] = i;
        }
        final_cost = this.calculeCost(0, path, this.n-1);
        constructTour();
    }
    private void constructTour(){
        /** Contrói o caminho utilizado. */
        int previous_set[] = new int[this.n-1], 
        next_set[] = new int[this.n-2];
        
        for(int i = 1; i < this.n; i++){
            previous_set[i-1] = i;
        }
        int size_set = n-1;        
        this.tour[0] = minimum(0, previous_set, size_set);
        for(int i = 1; i < n-1; i++){
            int k=0;
            for(int j = 0; j < size_set; j++){
                if(this.tour[i-1] != previous_set[j]){
                    //System.out.println("oii");
                    next_set[k++] = previous_set[j];
                }
            }
            --size_set;
            this.tour[i] = minimum(this.tour[i-1], next_set, size_set);
            for(int j = 0; j < size_set; j++){
                previous_set[j] = next_set[j];
            }            
        }
    }

    private int calculeCost(int current_node, int[] input_set, int size) {
        if(size == 0)
            return this.weight[current_node][0];
        int minimum = MIN;
        int minimum_index = 0;
        int set_to_be_passed_on_to_next_call_of_cost[] = new int[this.n-1];
        
        for(int i = 0; i < size; i++){
            int k=0;
            for(int j = 0; j < size; j++){
                if(input_set[i] != input_set[j])
                    set_to_be_passed_on_to_next_call_of_cost[k++] = input_set[j];
            }        
            int cost = calculeCost(input_set[i], set_to_be_passed_on_to_next_call_of_cost, size-1);
            if((this.weight[current_node][input_set[i]]+cost) < minimum){
                minimum = this.weight[current_node][input_set[i]]+cost;
                minimum_index = input_set[i];
            }
        }
        return minimum;
    }

    public void display(){
        System.out.println();
        System.out.print("O caminho é 1-");
        for(int i = 0; i < this.n-1; i++)
            System.out.print((this.tour[i]+1)+"-");
            System.out.print("1");
            System.out.println();
            System.out.println("O custo final é "+final_cost);    
    }
    private int minimum(int current_node, int[] input_set, int size) {
        if(size == 0)
            return this.weight[current_node][0];
        int minimum = MIN;
        int minimum_index = 0;
        int set_to_be_passed_on_to_next_call_of_cost[] = new int[this.n-1];
        
        for(int i = 0; i < size; i++){
            int k=0;
            for(int j = 0; j < size; j++){
                if(input_set[i] != input_set[j])
                    set_to_be_passed_on_to_next_call_of_cost[k++] = input_set[j];
            }        
            int cost = calculeCost(input_set[i], set_to_be_passed_on_to_next_call_of_cost, size-1);

            if((this.weight[current_node][input_set[i]]+cost) < minimum){
                minimum = this.weight[current_node][input_set[i]]+cost;
                minimum_index = input_set[i];
            }
        }
        return minimum_index;
    }
}
